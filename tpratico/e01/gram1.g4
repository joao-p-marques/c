grammar gram1;

@parser::members {
  public static SymbolTable symbolTable = new SymbolTable();
}

main: instr* EOF;

instr: print
     | assignment
     ;

print: 'print' expression 
     ;

assignment: ID ':' expression // #AssignExpression
          ;

expression: STR #ExprStr
          | ID  #ExprVar
          | 'input' '(' STR ')' #ExprInput
          | e1=expression '+' e2=expression #ExprConcat
          | '(' src=expression '/' target=expression '/' res=expression ')' #ExprReplace
          ;

STR: '"' .*? '"';
ID: [a-zA-Z_][a-zA-Z0-9]*;
WS: [\t\n\r ]+ -> skip;
COMMENT: '//' .*? '\n' -> skip;
ERROR: .; // tranform any lexical error in ERROR (no lexical errors)
