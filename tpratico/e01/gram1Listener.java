// Generated from gram1.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link gram1Parser}.
 */
public interface gram1Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link gram1Parser#main}.
	 * @param ctx the parse tree
	 */
	void enterMain(gram1Parser.MainContext ctx);
	/**
	 * Exit a parse tree produced by {@link gram1Parser#main}.
	 * @param ctx the parse tree
	 */
	void exitMain(gram1Parser.MainContext ctx);
	/**
	 * Enter a parse tree produced by {@link gram1Parser#instr}.
	 * @param ctx the parse tree
	 */
	void enterInstr(gram1Parser.InstrContext ctx);
	/**
	 * Exit a parse tree produced by {@link gram1Parser#instr}.
	 * @param ctx the parse tree
	 */
	void exitInstr(gram1Parser.InstrContext ctx);
	/**
	 * Enter a parse tree produced by {@link gram1Parser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(gram1Parser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link gram1Parser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(gram1Parser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link gram1Parser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(gram1Parser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link gram1Parser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(gram1Parser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprVar}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExprVar(gram1Parser.ExprVarContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprVar}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExprVar(gram1Parser.ExprVarContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprReplace}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExprReplace(gram1Parser.ExprReplaceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprReplace}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExprReplace(gram1Parser.ExprReplaceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprConcat}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExprConcat(gram1Parser.ExprConcatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprConcat}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExprConcat(gram1Parser.ExprConcatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprInput}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExprInput(gram1Parser.ExprInputContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprInput}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExprInput(gram1Parser.ExprInputContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprStr}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExprStr(gram1Parser.ExprStrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprStr}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExprStr(gram1Parser.ExprStrContext ctx);
}