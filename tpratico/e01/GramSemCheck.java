import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import static java.lang.System.*;
import java.util.Scanner;

public class GramSemCheck extends gram1BaseVisitor<String> {
	
        @Override public String visitPrint(gram1Parser.PrintContext ctx) { 
          String res = visit(ctx.expression());
          System.out.println(res);
          return res;
        }
	
        @Override public String visitAssignment(gram1Parser.AssignmentContext ctx) {
          String res = visit(ctx.expression());
          String id = ctx.ID().getText();
          if(res == null) {
            if (gram1Parser.symbolTable.exists(id)) {
              err.println("ERROR: variable " + id + " exists!");
              res = null;
            }
          }
          gram1Parser.symbolTable.put(id, res);
          return res;
        }
	
        @Override public String visitExprStr(gram1Parser.ExprStrContext ctx) { 
          String res = ctx.STR().getText();
          return res.substring(1, res.length()-1);
        }
	
        @Override public String visitExprVar(gram1Parser.ExprVarContext ctx) {
          String res = null;
          String id = ctx.ID().getText();
          res = gram1Parser.symbolTable.get(id);
          return res;
        }

        @Override public String visitExprInput(gram1Parser.ExprInputContext ctx) { 
          Scanner sc = new Scanner(System.in);

          String res = ctx.STR().getText();
          String nres = res.substring(1, res.length()-1);
          System.out.println(nres);
          
          String prompt = sc.nextLine();
          return prompt;
        }

	@Override public String visitExprConcat(gram1Parser.ExprConcatContext ctx) {
          String s1 = visit(ctx.e1);
          String s2 = visit(ctx.e2);

          return s1+s2;
        }

	@Override public String visitExprReplace(gram1Parser.ExprReplaceContext ctx) { 
          String src = visit(ctx.src);
          String target = visit(ctx.target);
          String res = visit(ctx.res);

          return src.replaceAll(target, res);
        }
}
