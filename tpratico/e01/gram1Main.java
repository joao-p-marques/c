import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.*;
import java.lang.System;

public class gram1Main {
   public static void main(String[] args) throws Exception {
      InputStream in_stream = null;
      String file_name = "prog";
      try {
        in_stream = new FileInputStream(new File(file_name));
      }
      catch (FileNotFoundException e) {
        System.err.println("ERROR: File not found!");
        System.exit(1);
      }

      // create a CharStream that reads from standard input:
      CharStream input = CharStreams.fromStream(in_stream);
      // create a lexer that feeds off of input CharStream:
      gram1Lexer lexer = new gram1Lexer(input);
      // create a buffer of tokens pulled from the lexer:
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      // create a parser that feeds off the tokens buffer:
      gram1Parser parser = new gram1Parser(tokens);
      // replace error listener:
      //parser.removeErrorListeners(); // remove ConsoleErrorListener
      //parser.addErrorListener(new ErrorHandlingListener());
      // begin parsing at main rule:
      ParseTree tree = parser.main();
      if (parser.getNumberOfSyntaxErrors() == 0) {
         // print LISP-style tree:
         // System.out.println(tree.toStringTree(parser));
         GramSemCheck visitor0 = new GramSemCheck();
         visitor0.visit(tree);
      }
   }
}
