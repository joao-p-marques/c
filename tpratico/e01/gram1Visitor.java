// Generated from gram1.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link gram1Parser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface gram1Visitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link gram1Parser#main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMain(gram1Parser.MainContext ctx);
	/**
	 * Visit a parse tree produced by {@link gram1Parser#instr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstr(gram1Parser.InstrContext ctx);
	/**
	 * Visit a parse tree produced by {@link gram1Parser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(gram1Parser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link gram1Parser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(gram1Parser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprVar}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprVar(gram1Parser.ExprVarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprReplace}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprReplace(gram1Parser.ExprReplaceContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprConcat}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprConcat(gram1Parser.ExprConcatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprInput}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprInput(gram1Parser.ExprInputContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprStr}
	 * labeled alternative in {@link gram1Parser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprStr(gram1Parser.ExprStrContext ctx);
}