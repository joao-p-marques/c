import java.lang.System.*;
import java.util.*;

public class SymbolTable {
  
  private Map<String, String> table = new HashMap<String, String>();  

  public boolean exists(String name) {
    assert(name != null);
    return table.containsKey(name);
  }

  public void put(String name, String value) {
    table.put(name, value);
  }

  public String get(String name) {
    return table.get(name);
  }

}

  
