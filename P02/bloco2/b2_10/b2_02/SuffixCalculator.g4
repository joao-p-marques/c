grammar SuffixCalculator;

program :
  stat* EOF
  ;

stat :
  expr NEWLINE
  { System.out.println($expr.res); }
  | NEWLINE
  ;

expr returns[double res=0] :
  one=expr two=expr op=('*'|'/'|'+'|'-')
  {
    switch ($op.text) {
      case "+":
        $res = $one.res + $two.res;
        break;
      case "-":
        $res = $one.res - $two.res;
        break;
      case "/":
        $res = $one.res / $two.res;
        break;
      case "*":
        $res = $one.res * $two.res;
        break;
    }
    System.out.print($one.res + " " + $op.text + " " + $two.res + " = ");
  }
  | Number
  { $res = Double.parseDouble($Number.text); }
  // {} for executing code as Java
  ;

Number : [0-9]+('.'[0-9]+)?;
NEWLINE : '\r'? '\n' ;
WS: [ \t]+ -> skip ;
