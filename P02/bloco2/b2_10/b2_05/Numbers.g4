
grammar Numbers;

@header {import java.util.*;}
@members {Map<String, Double> sym_map = new HashMap<>(); }

program:
  line* EOF
  ;

line:
  assignment NEWLINE
  | expr NEWLINE
  { System.out.println($expr.res); }
  | NEWLINE
  ;

assignment:
  NAME '=' expr
  { sym_map.put($NAME.text, $expr.res); }
  ;

expr returns[double res=0.0] :
  one=expr op=('*'|'/') two=expr
  {
    switch ($op.text) {
      case "/":
        $res = $one.res / $two.res;
        break;
      case "*":
        $res = $one.res * $two.res;
        break;
    }
    System.out.print($one.res + " " + $op.text + " " + $two.res + " = ");
  }
  | one=expr op=('+'|'-') two=expr
  {
    switch ($op.text) {
      case "+":
        $res = $one.res + $two.res;
        break;
      case "-":
        $res = $one.res - $two.res;
        break;
    }
    System.out.print($one.res + " " + $op.text + " " + $two.res + " = ");
  }
  | INT
  { $res = Double.parseDouble($INT.text); }
  | NAME
  { 
    if(sym_map.containsKey($NAME.text))
      $res = sym_map.get($NAME.text);
    else
      $res = 0.0;
  }
  | '(' expr ')'
  { $res = $expr.res; }
  ;
 

INT: [0-9]+;
NAME: [a-z]+;
NEWLINE: '\r'? '\n';
WS: [ \t]+ -> skip;
