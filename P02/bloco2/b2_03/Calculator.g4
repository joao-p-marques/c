grammar Calculator;

program :
  stat* EOF
  ;

stat :
  expr NEWLINE
  { System.out.println($expr.res); }
  | NEWLINE
  ;

expr returns[double res=0] :
  one=expr op=('*'|'/') two=expr
  {
    switch ($op.text) {
      case "/":
        $res = $one.res / $two.res;
        break;
      case "*":
        $res = $one.res * $two.res;
        break;
    }
    System.out.print($one.res + " " + $op.text + " " + $two.res + " = ");
  }
  | one=expr op=('+'|'-') two=expr
  {
    switch ($op.text) {
      case "+":
        $res = $one.res + $two.res;
        break;
      case "-":
        $res = $one.res - $two.res;
        break;
    }
    System.out.print($one.res + " " + $op.text + " " + $two.res + " = ");
  }
  | INT
  { $res = Double.parseDouble($INT.text); }
  | '(' expr ')'
  { $res = $expr.res; }
  ;

INT: [0-9]+;
NEWLINE: '\r'? '\n';
WS: [ \t]+ -> skip;


