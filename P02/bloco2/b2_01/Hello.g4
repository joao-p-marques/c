grammar Hello;

sentence: (greetings | bye)+ ;

greetings : 'ola'  ID { System.out.println("Hi " + $ID.text); };
bye : 'adeus' ID { System.out.println("Bye " + $ID.text); };

ID : [a-zA-Z]+ ;
WS : [ \t\r\n]+ -> skip ; 
