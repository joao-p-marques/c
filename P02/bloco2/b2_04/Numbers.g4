
grammar Numbers;

program:
  stat* EOF
  ;

stat:
  expr NEWLINE
  | NEWLINE
  ;

expr:
  INT '-' NAME
  ;

INT: [0-9]+;
NAME: [a-z]+;
NEWLINE: '\r'? '\n';
WS: [ \t]+ -> skip;
