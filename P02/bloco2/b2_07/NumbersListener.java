// Generated from Numbers.g4 by ANTLR 4.7.2
import java.util.*;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link NumbersParser}.
 */
public interface NumbersListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link NumbersParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(NumbersParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link NumbersParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(NumbersParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link NumbersParser#line}.
	 * @param ctx the parse tree
	 */
	void enterLine(NumbersParser.LineContext ctx);
	/**
	 * Exit a parse tree produced by {@link NumbersParser#line}.
	 * @param ctx the parse tree
	 */
	void exitLine(NumbersParser.LineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprNum}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExprNum(NumbersParser.ExprNumContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprNum}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExprNum(NumbersParser.ExprNumContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprBrac}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExprBrac(NumbersParser.ExprBracContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprBrac}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExprBrac(NumbersParser.ExprBracContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprMultDiv}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExprMultDiv(NumbersParser.ExprMultDivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprMultDiv}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExprMultDiv(NumbersParser.ExprMultDivContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprSumDiff}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExprSumDiff(NumbersParser.ExprSumDiffContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprSumDiff}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExprSumDiff(NumbersParser.ExprSumDiffContext ctx);
}