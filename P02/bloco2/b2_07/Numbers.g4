
grammar Numbers;

@header {import java.util.*;}

program:
  line* EOF
  ;

line:
  expression NEWLINE
  /* { System.out.println($expression.res); } */
  | NEWLINE
  ;

expression: 
  one=expression op=('*'|'/') two=expression #ExprSumDiff
  | one=expression op=('+'|'-') two=expression #ExprMultDiv
  | INT #ExprNum
  | '(' expression ')' #ExprBrac
  ;


INT: [0-9]+;
NEWLINE: '\r'? '\n';
WS: [ \t]+ -> skip;
