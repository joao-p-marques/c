// Generated from Numbers.g4 by ANTLR 4.7.2
import java.util.*;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link NumbersParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface NumbersVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link NumbersParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(NumbersParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link NumbersParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLine(NumbersParser.LineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprNum}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprNum(NumbersParser.ExprNumContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprBrac}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprBrac(NumbersParser.ExprBracContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprMultDiv}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprMultDiv(NumbersParser.ExprMultDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprSumDiff}
	 * labeled alternative in {@link NumbersParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprSumDiff(NumbersParser.ExprSumDiffContext ctx);
}