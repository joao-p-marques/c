#!/bin/bash

echo Making "$1"

antlr4 $1.g4 -visitor
cp $1BaseVisitor.java Infix2Postfix.java
antlr4-main $1 program -v Infix2Postfix 
# antlr4-build
