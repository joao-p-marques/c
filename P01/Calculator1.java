import java.util.Scanner;

class Calculator1 {

  public static void main (String[] args) {
    
    double op1 = 0, op2 = 0, res = 0;
    String opnd;

    Scanner input = new Scanner(System.in);

    if(input.hasNextDouble())
      op1 = input.nextDouble();
    else {
      System.err.println("Operand 1 is invalid");
      throw new IllegalArgumentException("Operand 1 is invalid");
    }
    
    if(input.hasNext())
      opnd = input.next();
    else {
      System.err.println("Operator is invalid");
      throw new IllegalArgumentException("Operator is invalid");
    }

    if(input.hasNextDouble())
      op2 = input.nextDouble();
    else {
      System.err.println("Operand 2 is invalid");
      throw new IllegalArgumentException("Operand 2 is invalid");
    }


    if (opnd.equals("+")) 
      res = op1 + op2;
    else if (opnd.equals("-"))
      res = op1 - op2;
    else if (opnd.equals("*"))
      res = op1 * op2;
    else if (opnd.equals("/"))
      res = op1 / op2;

    System.out.println(res);

    input.close();

  }


}  
