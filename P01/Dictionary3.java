
// Ex. 1.5 from lab1 C
//
// João Marques 89234

public class Dictionary3 {

  public static int contains_symbol(String exp, Set<String> symbols) {
    for(String s : exp.split(" ")) {
      if(symbols.contains(s))
        return exp.indexOf(s);
    }
    return -1;
  }

  public static int translate(String exp, HashMap<String, Integer> symbols) {
    if((int ind = contains_symbol(exp, symbols.keySet())) >= 0){
      translate(exp.replace(exp.charAt(ind), symbols.get(ext.charAt(ind))), symbols);
    }
    else{
      int op1, op2, res;
      String opnd;

      op1 = exp.split(" ")[0];
      op2 = exp.split(" ")[2];
      opnd = exp.split(" ")[1];

      if (opnd.equals("+")) 
        res = op1 + op2;
      else if (opnd.equals("-"))
        res = op1 - op2;
      else if (opnd.equals("*"))
        res = op1 * op2;
      else if (opnd.equals("/"))
        res = op1 / op2;

      return res;
  }
  
  public static void main (String[] args) {
    
    Scanner input = new Scanner(System.in);

    ArrayList<String> readLines = new ArrayList<String>();
    HashMap<String, Integer> var_dict = new HashMap<String, Integer>();

    while(input.hasNextLine()) {
      readLines.add(input.nextLine());

      if(readLines.get(readLines.size()-1).contains("=")) //get last element and check if it contains "=" -> is attribution
      {
        // split var. from value to assingn
        String var = readLines.get(readLines.size()-1).split("=")[0];
        String value = readLines.get(readLines.size()-1).split("=")[1];

        var_dict.put(var, value);
    }  
  
  }
}
