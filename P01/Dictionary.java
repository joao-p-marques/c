import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.ArrayList;
// import java.util.Arrays;

class Dictionary {

  public static void main (String[] args) throws FileNotFoundException {

    HashMap<String, Integer> number_dict = new HashMap<String, Integer>();
        
    ArrayList<String> word_list = new ArrayList<String>();
    ArrayList<String> output_string = new ArrayList<String>();

    Scanner input = new Scanner(new FileReader("numbers.txt"));

    int n;
    String name;
    String tmp; 

    //read file dict
    while(input.hasNext()) {
      n = input.nextInt(); // read number
      tmp = input.next(); // read "-" that separates number from word
      name = input.next(); // read number as a word

      number_dict.put(name, n);
    }        

    input.close(); // close file "numbers.txt"

    input = new Scanner(System.in);
    
    while(input.hasNext()) {
      tmp = input.next();
      if(!tmp.contains("-"))
        word_list.add(tmp);
      else {
        // word_list.add(Arrays.asList(tmp.split("-")));
        for(String s : tmp.split("-"))
          word_list.add(s);
      }
    }

    input.close(); // close input
      
    for(String s : word_list) {
      try {
        output_string.add(number_dict.get(s).toString());
      } catch (NullPointerException npe) {
        output_string.add(s);
      }
    }

    for(String s : output_string) 
      System.out.print(s + " ");

    System.out.println();
      
    }
}
