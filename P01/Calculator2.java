import java.util.Scanner;
import java.util.Stack;

class Calculator2 {

  public static void read_ops(Stack<Double> ops, Scanner input) {
    while (input.hasNextDouble()) {
      ops.push(input.nextDouble());
      System.out.println("Stack: " + ops);
    }
  }
  
  public static void main (String[] args) {
    
    Stack<Double> ops = new Stack<Double>();
    double res = 0;
    String opnd;
    
    Scanner input = new Scanner(System.in);

    while(input.hasNext()) {
      read_ops(ops, input);
      opnd = input.next();

      if (ops.size() < 2) {
        throw new IllegalArgumentException("Illegal Expression");
      }

      if (opnd.equals("+")) 
        res = ops.pop() + ops.pop();
      else if (opnd.equals("-"))
        res = ops.pop() - ops.pop();
      else if (opnd.equals("*"))
        res = ops.pop() * ops.pop();
      else if (opnd.equals("/"))
        res = ops.pop() / ops.pop();
      else {
        throw new IllegalArgumentException("Invalid Operator");
      }

      ops.push(res);

      System.out.println("Stack: " + ops);
      
      // if(ops.size() == 1)
      //   break;
    }

    System.out.println(ops.pop());

    input.close();

  }


}  
