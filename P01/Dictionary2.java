import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
// import java.util.Arrays;

class Dictionary2 {

  public static void main (String[] args) throws FileNotFoundException {

    HashMap<String, Integer> number_dict = new HashMap<String, Integer>();
    HashMap<String, Integer> multipliers_dict = new HashMap<String, Integer>();
        
    ArrayList<String> word_list = new ArrayList<String>();
    ArrayList<String> output_string = new ArrayList<String>();

    Scanner input = new Scanner(new FileReader("numbers.txt"));

    int n;
    String name;
    String tmp; 

    //read file dict
    while(input.hasNext()) {
      n = input.nextInt(); // read number
      tmp = input.next(); // read "-" that separates number from word
      name = input.next(); // read number as a word

      if(name.equals("hundred") || name.equals("thousand") || name.equals("million")) 
        multipliers_dict.put(name, n);
      else
        number_dict.put(name, n);

      // number_dict.put(name, n);
    }        

    input.close(); // close file "numbers.txt"

    input = new Scanner(System.in);
    
    while(input.hasNext()) {
      tmp = input.next();
      if(!tmp.contains("-"))
        word_list.add(tmp);
      else {
        // word_list.add(Arrays.asList(tmp.split("-")));
        for(String s : tmp.split("-"))
          word_list.add(s);
      }
    }

    input.close(); // close input

    System.out.println(number_dict);
    System.out.println();
    System.out.println(multipliers_dict);
    System.out.println();
      
    int number = 0, tmp_number = 0;
    tmp_number = number_dict.get(word_list.get(0));
    boolean last_was_mult = false;

    for (int i=1; i<word_list.size(); i++) {
      if(number_dict.containsKey(word_list.get(i))) { // if is a number
        // int cur_num = number_dict.get(word_list.get(i));
        // int prev_num = number_dict.get(word_list.get(i-1));


        if(last_was_mult) {
          number += tmp_number;
          tmp_number = 0;
        }
        tmp_number += number_dict.get(word_list.get(i));
;
      }
      else if (multipliers_dict.containsKey(word_list.get(i))) { // is a multiplier
        tmp_number = tmp_number * multipliers_dict.get(word_list.get(i));
        last_was_mult = true;
      }
        

      // if(cur_num < prev_num) { // if is a number
      //   number += tmp_number;
      //   if(last_was_mult)
      //     tmp_number = 0;
      //   tmp_number += cur_num;
      // }
      // else { // is a multiplier
      //   tmp_number = tmp_number * cur_num;
      //   last_was_mult = true;
      // }
    }
    number += tmp_number;

    // for(String s : output_string) 
    //   System.out.print(s + " ");

    System.out.println(number);
      
    }

}

